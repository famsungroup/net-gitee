﻿using Newtonsoft.Json;

namespace GiteeOpenSdk.Models
{
    /// <summary>
    /// 授权返回模型
    /// </summary>
    public class AccessTokenResponseModel : GiteeResponseModel
    {
        #region Success
        /// <summary>
        /// 授权类型
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        /// <summary>
        /// 授权码
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        /// <summary>
        /// 权限范围（空格隔开）
        /// </summary>
        [JsonProperty("scope")]
        public string Scope { get; set; }

        /// <summary>
        /// 刷新授权
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [JsonProperty("created_at")]
        public int CreatedAt { get; set; }
        #endregion
        #region Error
        /// <summary>
        /// 错误码
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }

        /// <summary>
        /// 错误描述
        /// </summary>
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
        #endregion
    }
}
