﻿using System;
using System.Collections.Generic;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Issues
{
    public class IssueBaseResponseModel : GiteeResponseModel
    {
        [JsonProperty("assignee")]
        public UserModel Assignee { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }
        [JsonProperty("body_html")]
        public string BodyHtml { get; set; }
        [JsonProperty("branch")]
        public string Branch { get; set; }
        [JsonProperty("collaborators")]
        public List<UserModel> Collaborators { get; set; }

        [JsonProperty("comments")]
        public int Comments { get; set; }
        [JsonProperty("comments_url")]
        public string CommentsUrl { get; set; }
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public DateTimeOffset? Deadline { get; set; }
        /// <summary>
        /// 所在层级
        /// </summary>
        [JsonProperty("depth")]
        public int Depth { get; set; }
        [JsonProperty("finished_at")]
        public DateTimeOffset? FinishedAt { get; set; }
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("issue_state")]
        public string IssueState { get; set; }
        [JsonProperty("issue_type")]
        public string IssueType { get; set; }
        [JsonProperty("labels")]
        public List<LabelModel> Labels { get; set; }
        [JsonProperty("labels_url")]
        public string LabelsUrl { get; set; }
        [JsonProperty("milestone")]
        public MilestoneModel Milestone { get; set; }
        [JsonProperty("number")]
        public string Number { get; set; }
        /// <summary>
        /// 上级 id
        /// </summary>
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }
        [JsonProperty("parent_url")]
        public string ParentUrl { get; set; }
        [JsonProperty("plan_started_at")]
        public DateTimeOffset? PlanStartedAt { get; set; }
        /// <summary>
        ///  优先级(0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重)
        /// </summary>
        [JsonProperty("priority")]
        public int Priority { get; set; }
        [JsonProperty("program")]
        public ProjectModel Program { get; set; }
        [JsonProperty("repository")]
        public RespositoryModel Repository { get; set; }
        [JsonProperty("repository_url")]
        public string RepositoryUrl { get; set; }
        [JsonProperty("scheduled_time")]
        public string ScheduledTime { get; set; }
        [JsonProperty("security_hole")]
        public bool SecurityHole { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("user")]
        public UserModel User { get; set; }
    }
}
