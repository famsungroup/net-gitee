﻿using System;
using System.Collections.Generic;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Issues
{
    /// <summary>
    /// 获取企业的某个Issue
    /// </summary>
    public class GetV5EnterprisesEnterpriseIssuesNumberResponseModel : IssueBaseResponseModel
    {
    }
}