﻿using GiteeOpenSdk.Models.BaseModels;

namespace GiteeOpenSdk.Models.Response.Respositories
{
    /// <summary>
    /// 创建分支返回模型
    /// </summary>
    public class PostV5ReposOwnerRepoBranchesResponseModel : BranchModel
    {
    }
}
