﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    /// <summary>
    /// 标签模型
    /// </summary>
    public class LabelModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("color")]
        public string Color { get; set; }
        [JsonProperty("repository_id")]
        public int RepositoryId { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
    }
}
