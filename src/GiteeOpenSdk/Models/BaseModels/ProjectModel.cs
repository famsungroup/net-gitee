﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    /// <summary>
    /// 项目模型
    /// </summary>
    public class ProjectModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("assignee")]
        public UserModel Assignee { get; set; }
        [JsonProperty("author")]
        public UserModel Author { get; set; }
    }
}
