﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    /// <summary>
    /// 命名空间模型
    /// </summary>
    public class NameSpaceModel : GiteeResponseModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("path")]
        public string Path { get; set; }
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
    }
}
