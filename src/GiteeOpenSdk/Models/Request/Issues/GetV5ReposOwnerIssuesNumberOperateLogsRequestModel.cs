using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取某个Issue下的操作日志
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerIssuesNumberOperateLogsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// Issue 编号(区分大小写，无需添加 # 号)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public string Number { get; set; }
        /// <summary>
        /// 按递增(asc)或递减(desc)排序，默认：递减
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }

    }
}