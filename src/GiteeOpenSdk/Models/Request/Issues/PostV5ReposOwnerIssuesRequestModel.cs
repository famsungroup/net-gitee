using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 创建Issue
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerIssuesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// Issue标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 企业自定义任务类型，非企业默认任务类型为“任务”
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_type")]
        public string IssueType { get; set; }
        /// <summary>
        /// Issue描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// Issue负责人的个人空间地址
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee")]
        public string Assignee { get; set; }
        /// <summary>
        /// Issue协助者的个人空间地址, 以 , 分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("collaborators")]
        public string Collaborators { get; set; }
        /// <summary>
        /// 里程碑序号
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("milestone")]
        public int Milestone { get; set; }
        /// <summary>
        /// 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("labels")]
        public string Labels { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program")]
        public string Program { get; set; }
        /// <summary>
        /// 是否是私有issue(默认为false)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("security_hole")]
        public bool SecurityHole { get; set; }

    }
}