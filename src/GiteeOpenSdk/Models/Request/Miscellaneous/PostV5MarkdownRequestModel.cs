using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Miscellaneous
{
    /// <summary>
    /// 渲染 Markdown 文本
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5MarkdownRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// Markdown 文本
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("text")]
        public string Text { get; set; }

    }
}