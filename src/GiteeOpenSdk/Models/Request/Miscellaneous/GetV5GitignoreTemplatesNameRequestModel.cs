using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Miscellaneous
{
    /// <summary>
    /// 获取一个 .gitignore 模板
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5GitignoreTemplatesNameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// .gitignore 模板名
        /// </summary>
        [RequestType("path")]
        [JsonProperty("name")]
        public string Name { get; set; }

    }
}