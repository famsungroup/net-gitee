using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Miscellaneous
{
    /// <summary>
    /// 获取一个开源许可协议
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5LicensesLicenseRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 协议名称
        /// </summary>
        [RequestType("path")]
        [JsonProperty("license")]
        public string License { get; set; }

    }
}