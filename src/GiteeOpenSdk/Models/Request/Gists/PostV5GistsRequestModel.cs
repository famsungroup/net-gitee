using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Gists
{
    /// <summary>
    /// 创建代码片段
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5GistsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// Hash形式的代码片段文件名以及文件内容。如: { &quot;file1.txt&quot;: { &quot;content&quot;: &quot;String file contents&quot; } }
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("files")]
        public object Files { get; set; }
        /// <summary>
        /// 代码片段描述，1~30个字符
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 公开/私有，默认: 私有
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("public")]
        public bool Public { get; set; }

    }
}