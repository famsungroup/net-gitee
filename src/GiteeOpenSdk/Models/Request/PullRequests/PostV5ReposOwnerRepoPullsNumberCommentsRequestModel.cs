using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 提交Pull Request评论
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoPullsNumberCommentsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 第几个PR，即本仓库PR的序数
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public int Number { get; set; }
        /// <summary>
        /// 必填。评论内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 可选。PR代码评论的commit id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("commit_id")]
        public string CommitId { get; set; }
        /// <summary>
        /// 可选。PR代码评论的文件名
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 可选。PR代码评论diff中的行数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("position")]
        public int Position { get; set; }

    }
}