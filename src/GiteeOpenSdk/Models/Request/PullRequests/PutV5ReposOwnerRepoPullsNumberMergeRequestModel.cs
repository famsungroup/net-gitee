using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 合并Pull Request
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5ReposOwnerRepoPullsNumberMergeRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 第几个PR，即本仓库PR的序数
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public int Number { get; set; }
        /// <summary>
        /// 可选。合并PR的方法，merge（合并所有提交）和 squash（扁平化分支合并）。默认为merge。
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("merge_method")]
        public string MergeMethod { get; set; }
        /// <summary>
        /// 可选。合并PR后是否删除源分支，默认false（不删除）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("prune_source_branch")]
        public bool PruneSourceBranch { get; set; }
        /// <summary>
        /// 可选。合并标题，默认为PR的标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 可选。合并描述，默认为 &quot;Merge pull request !{pr_id} from {author}/{source_branch}&quot;，与页面显示的默认一致。
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }

    }
}