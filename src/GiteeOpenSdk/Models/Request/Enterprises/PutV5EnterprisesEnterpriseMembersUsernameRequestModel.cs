using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 修改企业成员权限或备注
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5EnterprisesEnterpriseMembersUsernameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 企业角色：member =&gt; 普通成员, outsourced =&gt; 外包成员, admin =&gt; 管理员
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("role")]
        public string Role { get; set; }
        /// <summary>
        /// 是否可访问企业资源，默认:是。（若选否则禁止该用户访问企业资源）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("active")]
        public bool Active { get; set; }
        /// <summary>
        /// 企业成员真实姓名（备注）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }

    }
}