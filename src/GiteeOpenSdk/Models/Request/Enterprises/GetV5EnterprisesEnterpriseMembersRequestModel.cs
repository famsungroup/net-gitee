using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 列出企业的所有成员
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 根据角色筛选成员
        /// </summary>
        [RequestType("query")]
        [JsonProperty("role")]
        public string Role { get; set; }

    }
}