using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Users
{
    /// <summary>
    /// 列出授权用户所有的 Namespace
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UserNamespacesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 参与方式: project(所有参与仓库的namepsce)、intrant(所加入的namespace)、all(包含前两者)，默认(intrant)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("mode")]
        public string Mode { get; set; }

    }
}