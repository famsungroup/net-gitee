using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 新建仓库保护分支策略
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5ReposOwnerRepoBranchesSettingNewRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 分支/通配符
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("wildcard")]
        public string Wildcard { get; set; }
        /// <summary>
        /// admin: 仓库管理员, none: 禁止任何人合并; 用户: 个人的地址path(多个用户用 &#39;;&#39; 隔开)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pusher")]
        public string Pusher { get; set; }
        /// <summary>
        /// admin: 仓库管理员, none: 禁止任何人合并; 用户: 个人的地址path(多个用户用 &#39;;&#39; 隔开)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("merger")]
        public string Merger { get; set; }

    }
}