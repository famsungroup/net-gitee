using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 两个Commits之间对比的版本差异
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoCompareBaseHeadRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// Commit提交的SHA值或者分支名作为对比起点
        /// </summary>
        [RequestType("path")]
        [JsonProperty("base")]
        public string Base { get; set; }
        /// <summary>
        /// Commit提交的SHA值或者分支名作为对比终点
        /// </summary>
        [RequestType("path")]
        [JsonProperty("head")]
        public string Head { get; set; }

    }
}