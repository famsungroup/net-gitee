using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 发送私信给指定用户
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5NotificationsMessagesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 私信内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("content")]
        public string Content { get; set; }

    }
}