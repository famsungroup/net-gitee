using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 列出用户所属组织的动态
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UsersUsernameEventsOrgsOrgRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 滚动列表的最后一条记录的id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("prev_id")]
        public int PrevId { get; set; }
        /// <summary>
        /// 滚动列表每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("limit")]
        public int Limit { get; set; }
        /// <summary>
        /// 当前的页码(待废弃，建议使用滚动列表参数)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100(待废弃，建议使用滚动列表参数)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int PerPage { get; set; }
        /// <summary>
        /// 组织的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("org")]
        public string Org { get; set; }

    }
}