﻿using GiteeOpenSdk.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.Services
{
    /// <summary>
    /// 授权接口
    /// </summary>
    public class AuthApi : GiteeCommonApi
    {
        /// <summary>
        /// Token_Url
        /// </summary>
        public static readonly string TokenUrl = "https://gitee.com/oauth/token";

        public AuthApi() { }
        public AuthApi(string clientId, string clientSecret, string accessToken, string redirectUri)
            : base(clientId, clientSecret, accessToken)
        {
            RedirectUri = redirectUri;
        }

        /// <summary>
        /// 获取Token请求
        /// </summary>
        /// <param name="code"></param>
        /// <param name="redirectUri"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<ResponseModel<AccessTokenResponseModel>> GetAccessTokenAsync(string code/*, string state = null*/)
        {
            if (code != null)
            {
                // TODO 先读取未过期token，若已过期，则刷新或重新获取

                var data = new StringContent("", Encoding.UTF8);
                using (var hc = new HttpClient())
                {
                    var url = TokenUrl + $"?grant_type=authorization_code&code={code}&client_id={ClientId}&redirect_uri={RedirectUri}&client_secret={ClientSecret}";

                    var response = await hc.PostAsync(url, data);
                    string jsonString = await response.Content.ReadAsStringAsync();
                    //System.Console.WriteLine(jsonString);
                    var result = JsonConvert.DeserializeObject<AccessTokenResponseModel>(jsonString);
                    return new ResponseModel<AccessTokenResponseModel> { StatusCode = response.StatusCode, ResponseContent = result };
                }
            }
            return default;
        }

        /// <summary>
        /// 获取刷新Token请求
        /// </summary>
        /// <param name="refresh_token">刷新授权</param>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<ResponseModel<AccessTokenResponseModel>> GetRefreshTokenAsync(string refresh_token/*, string state = null*/)
        {
            if (refresh_token != null)
            {
                // TODO 先读取未过期token，若已过期，则刷新或重新获取

                var data = new StringContent("", Encoding.UTF8);
                using (var hc = new HttpClient())
                {
                    var url = TokenUrl + $"?grant_type=refresh_token&refresh_token={refresh_token}";

                    var response = await hc.PostAsync(url, data);
                    string jsonString = await response.Content.ReadAsStringAsync();
                    //System.Console.WriteLine(jsonString);
                    var result = JsonConvert.DeserializeObject<AccessTokenResponseModel>(jsonString);

                    return new ResponseModel<AccessTokenResponseModel> { StatusCode = response.StatusCode, ResponseContent = result };
                }
            }
            return default;
        }
    }
}
