﻿using GiteeOpenSdk.Models;
using GiteeOpenSdk.Models.Request.Repositories;
using GiteeOpenSdk.Models.Response.Respositories;
using System.Threading.Tasks;

namespace GiteeOpenSdk.Services
{
    /// <summary>
    /// 仓库接口
    /// </summary>
    public class RespositoriesApi : GiteeCommonApi
    {
        public RespositoriesApi() { }
        public RespositoriesApi(string clientId, string clientSecret, string accessToken) : base(clientId, clientSecret, accessToken) { }
        /// <summary>
        /// 创建分支
        /// </summary>
        public async Task<ResponseModel<PostV5ReposOwnerRepoBranchesResponseModel>> PostV5ReposOwnerRepoBranches(PostV5ReposOwnerRepoBranchesRequestModel model)
        {
            var result = await ExecuteAsync<PostV5ReposOwnerRepoBranchesRequestModel, PostV5ReposOwnerRepoBranchesResponseModel>("repos/{owner}/{repo}/branches", model);
            return result;
        }
        /// <summary>
        /// 获取所有分支
        /// </summary>
        public async Task<ResponseModels<GetV5ReposOwnerRepoBranchesResponseModel>> GetV5ReposOwnerRepoBranches(GetV5ReposOwnerRepoBranchesRequestModel model)
        {
            var result = await ExecutesAsync<GetV5ReposOwnerRepoBranchesRequestModel, GetV5ReposOwnerRepoBranchesResponseModel>("repos/{owner}/{repo}/branches", model);
            return result;
        }
    }
}
