﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GiteeOpenSdk.WebHook.Models
{
    /// <summary>
    /// Gitee WebHook Issue 返回模型
    /// </summary>
    public class WebHookIssueModel : WebHookBaseModel
    {
        /// <summary>
        /// 动作名称 state_change
        /// </summary>
        [JsonProperty("action")] 
        public string Action { get; set; }
        /// <summary>
        /// issue 信息。
        /// </summary>
        [JsonProperty("issue")]
        public Issue Issue { get; set; }
        /// <summary>
        /// 仓库信息。
        /// </summary>
        [JsonProperty("repository")]
        public Repository Repository { get; set; }
        /// <summary>
        /// 项目信息。
        /// </summary>
        [JsonProperty("project")]
        public Project Project { get; set; }
        /// <summary>
        /// 触发 hook 的用户信息。
        /// </summary>
        [JsonProperty("sender")]
        public User Sender { get; set; }
        /// <summary>
        /// 被委托处理 issue 的用户信息。
        /// </summary>
        [JsonProperty("target_user")]
        public User TargetUser { get; set; }
        /// <summary>
        /// issue 创建者。
        /// </summary>
        [JsonProperty("user")]
        public User User { get; set; }
        /// <summary>
        /// issue 负责人。
        /// </summary>
        [JsonProperty("assignee")]
        public User Assignee { get; set; }
        /// <summary>
        /// issue 负责人。
        /// </summary>
        [JsonProperty("updated_by")]
        public User UpdatedBy { get; set; }
        /// <summary>
        /// issue 对应的标识。eg：IG6E9
        /// </summary>
        [JsonProperty("iid")]
        public string Iid { get; set; }
        /// <summary>
        /// issue 标题。eg：这是一个 issue 标题
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// issue 的内容体。eg：数据库优化...
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// issue 状态。eg：open
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 里程碑的标题。eg：开源计划
        /// </summary>
        [JsonProperty("milestone")]
        public string Milestone { get; set; }
        /// <summary>
        /// issue 在 Gitee 上对应的 url。eg：https://gitee.com/oschina/git-osc/issues/1
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// issue 所属的企业信息。
        /// </summary>
        [JsonProperty("enterprise")]
        public Enterprise Enterprise { get; set; }
    }
}