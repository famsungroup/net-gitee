﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class Milestone
    {
        /// <summary>
        /// Gitee 上对应的 url。eg：https://gitee.com/oschina/git-osc/milestones/1
        /// </summary>
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 与上面的 id 一致
        /// </summary>
        [JsonProperty("number")]
        public int Number { get; set; }
        /// <summary>
        /// 里程碑的标题。eg：开源计划
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 里程碑的详细描述。eg：走向世界
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 开启状态的 issue 数量
        /// </summary>
        [JsonProperty("open_issues")]
        public int OpenIssues { get; set; }
        /// <summary>
        /// 关闭状态的 issue 数量
        /// </summary>
        [JsonProperty("closed_issues")]
        public int ClosedIssues { get; set; }
        /// <summary>
        /// 里程碑的状态。eg：open
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 里程碑创建的时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        /// <summary>
        /// 里程碑更新的时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        /// <summary>
        /// 里程碑结束的时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("due_on")]
        public string DueOn { get; set; }
    }
}
