﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class Issue
    {
        /// <summary>
        /// Gitee 上对应的 url。eg：https://gitee.com/oschina/git-osc/issues/1
        /// </summary>
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// issue 对应的标识。eg：IG6E9
        /// </summary>
        [JsonProperty("number")]
        public string Number { get; set; }
        /// <summary>
        /// issue 标题。eg：这是一个 issue 标题
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// issue 创建者。
        /// </summary>
        [JsonProperty("user")]
        public User User { get; set; }
        /// <summary>
        /// issue 对应的标签。
        /// </summary>
        [JsonProperty("labels")]
        public List<Label> Labels { get; set; }
        /// <summary>
        /// issue 状态。eg：open,progressing
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// issue 状态名。eg：代办的,开发中
        /// </summary>
        [JsonProperty("state_name")]
        public string StateName { get; set; }
        /// <summary>
        /// issue 类型。eg：任务 开发
        /// </summary>
        [JsonProperty("type_name")]
        public string TypeName { get; set; }
        /// <summary>
        /// issue 负责人。
        /// </summary>
        [JsonProperty("assignee")]
        public User Assignee { get; set; }
        /// <summary>
        /// issue 协助者。
        /// </summary>
        [JsonProperty("collaborators")]
        public List<User> Collaborators { get; set; }
        /// <summary>
        /// issue 所属的里程碑。
        /// </summary>
        [JsonProperty("milestone")]
        public Milestone milestone { get; set; }
        /// <summary>
        /// issue 的评论总数
        /// </summary>
        [JsonProperty("comments")]
        public int Comments { get; set; }
        /// <summary>
        /// issue 的创建时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        /// <summary>
        /// issue 的更新时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        /// <summary>
        /// issue 的内容体。eg：数据库优化...
        /// </summary>
        [JsonProperty("body")]
        public string Body { get; set; }
    }
}
