﻿using GiteeOpenSdk.Services;
using Microsoft.Extensions.Options;

namespace GiteeOpenSdk
{
    /// <summary>
    /// Gitee服务
    /// </summary>
    public class GiteeService
    {
        #region 接口分类
        /// <summary>
        /// 授权验证
        /// </summary>
        public AuthApi AuthApi { get; }
        public IssuesApi IssuesApi { get; }
        public RespositoriesApi RespositoriesApi { get; }
        public UsersApi UsersApi { get; }
        #endregion

        private readonly IOptions<GiteeOption> _option;

        public GiteeService(IOptions<GiteeOption> option)
        {
            _option = option;
            AuthApi = new AuthApi(_option.Value.ClientId, _option.Value.ClientSecret, _option.Value.AccessToken, _option.Value.RedirectUri);
            IssuesApi = new IssuesApi(_option.Value.ClientId, _option.Value.ClientSecret, _option.Value.AccessToken);
            RespositoriesApi = new RespositoriesApi(_option.Value.ClientId, _option.Value.ClientSecret, _option.Value.AccessToken);
            UsersApi = new UsersApi(_option.Value.ClientId, _option.Value.ClientSecret, _option.Value.AccessToken);
        }

        public GiteeService(GiteeOption option)
        {
            AuthApi = new AuthApi(option.ClientId, option.ClientSecret, option.AccessToken, option.RedirectUri);
            IssuesApi = new IssuesApi(option.ClientId, option.ClientSecret, option.AccessToken);
            RespositoriesApi = new RespositoriesApi(option.ClientId, option.ClientSecret, option.AccessToken);
            UsersApi = new UsersApi(option.ClientId, option.ClientSecret, option.AccessToken);
        }
        protected void SetToken(string accessToken)
        {
            AuthApi.AccessToken = accessToken;
            IssuesApi.AccessToken = accessToken;
            RespositoriesApi.AccessToken = accessToken;
            UsersApi.AccessToken = accessToken;
        }
    }
}