﻿using GiteeOpenSdk;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GiteeOpenSdk.Common;
using GiteeOpenSdk.Services;
using GiteeOpenSdk.WebHook;

namespace ConsoleSample
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello Leo.GiteeOpenSdk");
            var service = new GiteeService(new GiteeOption
            {
                ClientId = "ba81b1f7d52d4096cf2f9593e36abb48c21db58bae7bd25b3e1a050134609fdb",
                ClientSecret = "",
                RedirectUri = "https://abc.com/login"
            });

            var result = await service.AuthApi.GetAccessTokenAsync("f6c1fb1088e0ed4b1da4bb512b68fa459b26cd2e910117bf1a7eb0bd0e5afb16");
            Console.WriteLine($"GetAccessTokenAsync:StatusCode:{result.StatusCode} ResponseContent:{JsonConvert.SerializeObject(result.ResponseContent)}");
            if(result.StatusCode== System.Net.HttpStatusCode.OK)
            {
                result = await service.AuthApi.GetRefreshTokenAsync(result.ResponseContent.RefreshToken);
                Console.WriteLine($"GetRefreshTokenAsync:StatusCode:{result.StatusCode} ResponseContent:{JsonConvert.SerializeObject(result.ResponseContent)}");
            }

            {
                var GetEnterpriseIssueResult =await new IssuesApi().GetV5EnterprisesEnterpriseIssuesNumber(new GiteeOpenSdk.Models.Request.Issues.GetV5EnterprisesEnterpriseIssuesNumberRequestModel
                {
                    AccessToken = "",
                    Enterprise = "famsungroup",
                    Number = "I39BKD"
                });
                Console.WriteLine($"GetRefreshTokenAsync:StatusCode:{GetEnterpriseIssueResult.StatusCode} ResponseContent:{JsonConvert.SerializeObject(GetEnterpriseIssueResult.ResponseContent)}");
            }
            {
                var GetBranchesResult = await new RespositoriesApi().GetV5ReposOwnerRepoBranches(new GiteeOpenSdk.Models.Request.Repositories.GetV5ReposOwnerRepoBranchesRequestModel
                {
                    AccessToken = "",
                    Owner = "famsungroup",
                    Repo = "mes-main"
                });
                Console.WriteLine($"GetRefreshTokenAsync:StatusCode:{GetBranchesResult.StatusCode} ResponseContent:{JsonConvert.SerializeObject(GetBranchesResult.ResponseContent)}");
            }
            {
                var post = await new RespositoriesApi().PostV5ReposOwnerRepoBranches(new GiteeOpenSdk.Models.Request.Repositories.PostV5ReposOwnerRepoBranchesRequestModel
                {
                    AccessToken = "",
                    Owner = "famsungroup",
                    Repo = "demo-warehouse-1",
                    BranchName = "feature/Leo-I38GE3",
                    Refs = "master"
                });
                Console.WriteLine($"PostV5ReposOwnerRepoBranches:StatusCode:{post.StatusCode} ResponseContent:{JsonConvert.SerializeObject(post.ResponseContent)}");
            }
            {
                var sign = SignVerification.CreateSign("", 1614583950921);
                Console.WriteLine($"CreateSign:{sign}");
            }
            {
                var isSame = SignVerification.CompareSign("", 1614583950921, "niK2hmVY6yje1izK/b+HNkFhExJDrqP3hV3SjyBFDf0=");
                Console.WriteLine($"CompareSign:{isSame}");
            }
            {
                //var webhookitem = JsonConvert.DeserializeObject<GiteeOpenSdk.WebHook.Models.WebHookModel>("{\"action\":\"open\",\"issue\":{\"html_url\":\"https://gitee.com/oschina/git-osc/issues/I1EL99\",\"id\":295024870,\"number\":\"I1EL99\",\"title\":\"这是一条测试 WebHook 接收功能触发的推送\",\"user\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"labels\":[{\"id\":827033694,\"name\":\"bug\",\"color\":\"d73a4a\"}],\"state\":\"open\",\"state_name\":\"待办的\",\"type_name\":\"任务\",\"assignee\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"collaborators\":[{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"}],\"milestone\":{\"html_url\":\"https://gitee.com/oschina/gitee/milestones/14143\",\"id\":1,\"number\":1,\"title\":\"问题反馈\",\"description\":null,\"open_issues\":13,\"started_issues\":6,\"closed_issues\":31,\"approved_issues\":42,\"state\":\"open\",\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"due_on\":null},\"comments\":0,\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"body\":\"这是一条测试 WebHook 接收功能触发的推送\"},\"repository\":{\"id\":151,\"name\":\"Gitee FeedBack\",\"path\":\"git-osc\",\"full_name\":\"oschina/git-osc\",\"owner\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"private\":false,\"html_url\":\"https://gitee.com/oschina/git-osc\",\"url\":\"https://gitee.com/oschina/git-osc\",\"description\":\"\",\"fork\":false,\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"pushed_at\":\"2020-04-15T21:09:40+08:00\",\"git_url\":\"git://gitee.com:oschina/git-osc.git\",\"ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"clone_url\":\"https://gitee.com/oschina/git-osc.git\",\"svn_url\":\"svn://gitee.com/oschina/git-osc\",\"git_http_url\":\"https://gitee.com/oschina/git-osc.git\",\"git_ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"git_svn_url\":\"svn://gitee.com/oschina/git-osc\",\"homepage\":null,\"stargazers_count\":11,\"watchers_count\":12,\"forks_count\":0,\"language\":\"ruby\",\"has_issues\":true,\"has_wiki\":true,\"has_pages\":false,\"license\":null,\"open_issues_count\":0,\"default_branch\":\"master\",\"namespace\":\"oschina\",\"name_with_namespace\":\"OSCHINA/git-osc\",\"path_with_namespace\":\"oschina/git-osc\"},\"project\":{\"id\":151,\"name\":\"Gitee FeedBack\",\"path\":\"git-osc\",\"full_name\":\"oschina/git-osc\",\"owner\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"private\":false,\"html_url\":\"https://gitee.com/oschina/git-osc\",\"url\":\"https://gitee.com/oschina/git-osc\",\"description\":\"\",\"fork\":false,\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"pushed_at\":\"2020-04-15T21:09:40+08:00\",\"git_url\":\"git://gitee.com:oschina/git-osc.git\",\"ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"clone_url\":\"https://gitee.com/oschina/git-osc.git\",\"svn_url\":\"svn://gitee.com/oschina/git-osc\",\"git_http_url\":\"https://gitee.com/oschina/git-osc.git\",\"git_ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"git_svn_url\":\"svn://gitee.com/oschina/git-osc\",\"homepage\":null,\"stargazers_count\":11,\"watchers_count\":12,\"forks_count\":0,\"language\":\"ruby\",\"has_issues\":true,\"has_wiki\":true,\"has_pages\":false,\"license\":null,\"open_issues_count\":0,\"default_branch\":\"master\",\"namespace\":\"oschina\",\"name_with_namespace\":\"OSCHINA/git-osc\",\"path_with_namespace\":\"oschina/git-osc\"},\"sender\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"target_user\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"user\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"assignee\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"updated_by\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"iid\":\"I1EL99\",\"title\":\"这是一条测试 WebHook 接收功能触发的推送\",\"description\":\"这是一条测试 WebHook 接收功能触发的推送\",\"state\":\"open\",\"milestone\":\"问题反馈\",\"url\":\"https://gitee.com/oschina/git-osc/issues/I1EL99\",\"enterprise\":{\"name\":\"OSCHINA\",\"url\":\"https://gitee.com/oschina\"},\"hook_name\":\"issue_hooks\",\"hook_id\":561147,\"hook_url\":\"https://gitee.com/famsungroup/admin/hooks/561147/edit\",\"password\":\"\",\"timestamp\":\"1614581179249\",\"sign\":\"\"}");
                //Console.WriteLine(webhookitem);
            }
            {
                //var webhookitem = JsonConvert.DeserializeObject<GiteeOpenSdk.WebHook.Models.WebHookModel>("{\"action\":\"open\",\"issue\":{\"html_url\":\"https://gitee.com/oschina/git-osc/issues/I1EL99\",\"id\":295024870,\"number\":\"I1EL99\",\"title\":\"这是一条测试 WebHook 接收功能触发的推送\",\"user\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"labels\":[{\"id\":827033694,\"name\":\"bug\",\"color\":\"d73a4a\"}],\"state\":\"open\",\"state_name\":\"待办的\",\"type_name\":\"任务\",\"assignee\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"collaborators\":[{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"}],\"milestone\":{\"html_url\":\"https://gitee.com/oschina/gitee/milestones/14143\",\"id\":1,\"number\":1,\"title\":\"问题反馈\",\"description\":null,\"open_issues\":13,\"started_issues\":6,\"closed_issues\":31,\"approved_issues\":42,\"state\":\"open\",\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"due_on\":null},\"comments\":0,\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"body\":\"这是一条测试 WebHook 接收功能触发的推送\"},\"repository\":{\"id\":151,\"name\":\"Gitee FeedBack\",\"path\":\"git-osc\",\"full_name\":\"oschina/git-osc\",\"owner\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"private\":false,\"html_url\":\"https://gitee.com/oschina/git-osc\",\"url\":\"https://gitee.com/oschina/git-osc\",\"description\":\"\",\"fork\":false,\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"pushed_at\":\"2020-04-15T21:09:40+08:00\",\"git_url\":\"git://gitee.com:oschina/git-osc.git\",\"ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"clone_url\":\"https://gitee.com/oschina/git-osc.git\",\"svn_url\":\"svn://gitee.com/oschina/git-osc\",\"git_http_url\":\"https://gitee.com/oschina/git-osc.git\",\"git_ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"git_svn_url\":\"svn://gitee.com/oschina/git-osc\",\"homepage\":null,\"stargazers_count\":11,\"watchers_count\":12,\"forks_count\":0,\"language\":\"ruby\",\"has_issues\":true,\"has_wiki\":true,\"has_pages\":false,\"license\":null,\"open_issues_count\":0,\"default_branch\":\"master\",\"namespace\":\"oschina\",\"name_with_namespace\":\"OSCHINA/git-osc\",\"path_with_namespace\":\"oschina/git-osc\"},\"project\":{\"id\":151,\"name\":\"Gitee FeedBack\",\"path\":\"git-osc\",\"full_name\":\"oschina/git-osc\",\"owner\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"private\":false,\"html_url\":\"https://gitee.com/oschina/git-osc\",\"url\":\"https://gitee.com/oschina/git-osc\",\"description\":\"\",\"fork\":false,\"created_at\":\"2020-04-15T21:09:40+08:00\",\"updated_at\":\"2020-04-15T21:09:40+08:00\",\"pushed_at\":\"2020-04-15T21:09:40+08:00\",\"git_url\":\"git://gitee.com:oschina/git-osc.git\",\"ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"clone_url\":\"https://gitee.com/oschina/git-osc.git\",\"svn_url\":\"svn://gitee.com/oschina/git-osc\",\"git_http_url\":\"https://gitee.com/oschina/git-osc.git\",\"git_ssh_url\":\"git@gitee.com:oschina/git-osc.git\",\"git_svn_url\":\"svn://gitee.com/oschina/git-osc\",\"homepage\":null,\"stargazers_count\":11,\"watchers_count\":12,\"forks_count\":0,\"language\":\"ruby\",\"has_issues\":true,\"has_wiki\":true,\"has_pages\":false,\"license\":null,\"open_issues_count\":0,\"default_branch\":\"master\",\"namespace\":\"oschina\",\"name_with_namespace\":\"OSCHINA/git-osc\",\"path_with_namespace\":\"oschina/git-osc\"},\"sender\":{\"login\":\"oschina-org\",\"avatar_url\":\"https://gitee.com/assets/favicon.ico\",\"html_url\":\"https://gitee.com/oschina-org\",\"type\":\"User\",\"site_admin\":false,\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"target_user\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"user\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"assignee\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"updated_by\":{\"id\":1,\"name\":\"Gitee\",\"email\":\"gitee@gitee.com\",\"username\":\"oschina-org\",\"user_name\":\"Gitee\",\"url\":\"https://gitee.com/oschina-org\"},\"iid\":\"I1EL99\",\"title\":\"这是一条测试 WebHook 接收功能触发的推送\",\"description\":\"这是一条测试 WebHook 接收功能触发的推送\",\"state\":\"open\",\"milestone\":\"问题反馈\",\"url\":\"https://gitee.com/oschina/git-osc/issues/I1EL99\",\"enterprise\":{\"name\":\"OSCHINA\",\"url\":\"https://gitee.com/oschina\"},\"hook_name\":\"issue_hooks\",\"hook_id\":561147,\"hook_url\":\"https://gitee.com/famsungroup/admin/hooks/561147/edit\",\"password\":\"\",\"timestamp\":\"1614581179249\",\"sign\":\"\"}");
                //Console.WriteLine(webhookitem);

                var updateStatue = await new IssuesApi().PatchV5EnterprisesEnterpriseIssuesNumber(new GiteeOpenSdk.Models.Request.Issues.PatchV5EnterprisesEnterpriseIssuesNumberRequestModel
                {
                    AccessToken = "",
                    Enterprise = "famsungroup",
                    Number = "I39PR2",
                    Branch = "feature/Leo-I39PR2"
                });
                Console.WriteLine(updateStatue);
            }
            Console.WriteLine("Finish!");
            Console.ReadLine();
        }
    }
}