result=''
with open('test.txt','r') as f:
    for line in f.readlines():
        property_name_temp=line.strip().split(':')[0][1:-1]
        if '_' in property_name_temp:
            property_name=''
            for temp in property_name_temp.split('_'):
                property_name+=temp.capitalize()
        else:
            property_name=property_name_temp.capitalize()
        property_type_temp=line.strip().split(':')[1].strip()
        if property_type_temp=='string':
            property_type='string'
        elif property_type_temp=='integer':
            property_type='int'
        else:
            property_type='?'
        result+=f'[JsonProperty("{property_name_temp}")]\npublic {property_type} {property_name} {{get;set;}}\n'
        #print(result)
        
with open('result.txt','w') as f:
    f.write(result)