result=''
with open('test.txt','r') as f:
    temp_string=f.readlines()
    diff=0
    while (diff*5)<len(temp_string) :
        property_name_temp=temp_string[diff*5+0].strip().strip('*')
        property_description=temp_string[diff*5+2].strip()
        property_request_type_temp=temp_string[diff*5+3].strip()
        property_type_temp=temp_string[diff*5+4].strip()

        if property_request_type_temp!='path':
            #name
            if '_' in property_name_temp:
                property_name=''
                for temp in property_name_temp.split('_'):
                    property_name+=temp.capitalize()
            else:
                property_name=property_name_temp.capitalize()

            #type
            if property_type_temp=='string':
                property_type='string'
            elif property_type_temp=='integer':
                property_type='int'
            elif property_type_temp=='boolean':
                property_type='bool'
            else:
                property_type='?'
            result+=f'/// <summary>\n/// {property_description}\n/// </summary>\n[JsonProperty("{property_name_temp}")]\npublic {property_type} {property_name} {{get;set;}}\n'
        diff+=1
        
with open('result.txt','w') as f:
    f.write(result)