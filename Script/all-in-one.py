import json,os

with open('all.json','r') as f:
    datas=json.load(f)
#遍历所有api
for api,methods in datas['paths'].items():
    #遍历api下所有methods的key
    for method in methods.keys():
        method_name=method
        detail=methods[method]
        description=detail['description']
        tag=detail['tags'][0].replace(' ','')
        class_name="".join(detail['operationId'][:1].upper() + detail['operationId'][1:])
        if not os.path.exists(tag) :
            os.mkdir(tag)
        content=f'''
using System;
using GiteeOpenSdk.Common;
using Newtonsoft.Json;
namespace GiteeOpenSdk.Models.Request.{tag}
{{
    /// <summary>
    /// {description}
    /// </summary>
    [RequestMethod("{method.upper()}")]
    public class {class_name} : GiteeRequestModel
    {{
'''
        for parameter in detail['parameters']:
            property_description=parameter.get('description','None')
            property_required=parameter['required']
            property_request_type=parameter['in']
            property_type_temp=parameter['type']
            property_name_temp=parameter['name']

            #name
            if '_' in property_name_temp:
                property_name=''
                for temp in property_name_temp.split('_'):
                    property_name+=temp.capitalize()
            else:
                property_name=property_name_temp.capitalize()

            #type
            if property_type_temp=='string':
                property_type='string'
            elif property_type_temp=='integer':
                property_type='int'
                if not property_required:
                    property_type+='?'
            elif property_type_temp=='boolean':
                property_type='bool'
                if not property_required:
                    property_type+='?'
            else:
                property_type='unknow'
                if not property_required:
                    property_type+='?'
            
            content+=f'''
        /// <summary>
        /// {property_description}
        /// </summary>
        [RequestType("{property_request_type}")]
        [JsonProperty("{property_name_temp}")]
        public {property_type} {property_name} {{ get; set; }}
'''
        content+='''
    }
}
'''
        with open(f'{tag}/{class_name}.cs','w') as f:
            f.write(content)
        


        
